# translation of plasma_applet_lockout.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.es>, 2009.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2009, 2011.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2012, 2014.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2013, 2015.
# SPDX-FileCopyrightText: 2023 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_lockout\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-19 01:53+0000\n"
"PO-Revision-Date: 2023-10-21 10:58+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.2\n"

#: contents/config/config.qml:13
#, kde-format
msgid "General"
msgstr "Xeral"

#: contents/ui/ConfigGeneral.qml:33
#, kde-format
msgctxt ""
"Heading for a list of actions (leave, lock, switch user, hibernate, suspend)"
msgid "Show actions:"
msgstr "Amosar as accións:"

#: contents/ui/ConfigGeneral.qml:34
#, kde-format
msgid "Log Out"
msgstr "Saír"

#: contents/ui/ConfigGeneral.qml:41
#, kde-format
msgid "Shut Down"
msgstr "Apagar"

#: contents/ui/ConfigGeneral.qml:47
#, kde-format
msgid "Restart"
msgstr "Reiniciar"

#: contents/ui/ConfigGeneral.qml:53 contents/ui/data.js:5
#, kde-format
msgid "Lock"
msgstr "Trancar"

#: contents/ui/ConfigGeneral.qml:59
#, kde-format
msgid "Switch User"
msgstr "Cambiar de persoa usuaria"

#: contents/ui/ConfigGeneral.qml:65 contents/ui/data.js:46
#, kde-format
msgid "Hibernate"
msgstr "Hibernar"

#: contents/ui/ConfigGeneral.qml:71 contents/ui/data.js:39
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Durmir"

#: contents/ui/data.js:6
#, kde-format
msgid "Lock the screen"
msgstr "Trancar a pantalla"

#: contents/ui/data.js:12
#, kde-format
msgid "Switch user"
msgstr "Cambiar de persoa usuaria"

#: contents/ui/data.js:13
#, kde-format
msgid "Start a parallel session as a different user"
msgstr "Comezar unha sesión paralela cunha persoa usuaria distinta."

#: contents/ui/data.js:18
#, kde-format
msgid "Shutdown…"
msgstr "Apagar…"

#: contents/ui/data.js:19
#, kde-format
msgid "Turn off the computer"
msgstr "Apagar o computador"

#: contents/ui/data.js:25
#, kde-format
msgid "Restart…"
msgstr "Reiniciar…"

#: contents/ui/data.js:26
#, kde-format
msgid "Reboot the computer"
msgstr "Reiniciar o computador"

#: contents/ui/data.js:32
#, kde-format
msgid "Logout…"
msgstr "Saír…"

#: contents/ui/data.js:33
#, kde-format
msgid "End the session"
msgstr "Rematar a sesión"

#: contents/ui/data.js:40
#, kde-format
msgid "Sleep (suspend to RAM)"
msgstr "Suspender (en memoria)"

#: contents/ui/data.js:47
#, kde-format
msgid "Hibernate (suspend to disk)"
msgstr "Hibernar (suspender en disco)"

#~ msgid "Logout"
#~ msgstr "Saír"

#~ msgid "Reboot"
#~ msgstr "Reiniciar"

#~ msgid "Do you want to suspend to disk (hibernate)?"
#~ msgstr "Quere suspender no disco (hibernar)?"

#~ msgid "Yes"
#~ msgstr "Si"

#~ msgid "No"
#~ msgstr "Non"

#~ msgid "Do you want to suspend to RAM (sleep)?"
#~ msgstr "Quere suspender na RAM (durmir)?"

#~ msgid "Leave"
#~ msgstr "Saír"

#~ msgid "Leave..."
#~ msgstr "Saír…"

#~ msgctxt "Heading for list of actions (leave, lock, shutdown, ...)"
#~ msgid "Actions"
#~ msgstr "Accións"

#~ msgid "Suspend"
#~ msgstr "Suspender"

#~ msgid "Lock/Logout Settings"
#~ msgstr "Configuración de iniciar e saír da sesión"

#~ msgid "SystemTray Settings"
#~ msgstr "Configuración da bandexa do sistema"

#~ msgid "Configure Lock/Logout"
#~ msgstr "Configurar o tranque/saída"

#~ msgid "Please select one or more items on the list below."
#~ msgstr "Escolla un ou máis elementos na lista en baixo."
