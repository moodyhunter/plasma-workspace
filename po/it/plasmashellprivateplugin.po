# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Luigi Toscano <luigi.toscano@tiscali.it>, 2015, 2016, 2018, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-29 01:38+0000\n"
"PO-Revision-Date: 2023-11-23 14:25+0100\n"
"Last-Translator: Luigi Toscano <luigi.toscano@tiscali.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Luigi Toscano"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "luigi.toscano@tiscali.it"

#: calendar/eventdatadecorator.cpp:51
#, kde-format
msgctxt "Agenda listview section title"
msgid "Holidays"
msgstr "Festività"

#: calendar/eventdatadecorator.cpp:53
#, kde-format
msgctxt "Agenda listview section title"
msgid "Events"
msgstr "Eventi"

#: calendar/eventdatadecorator.cpp:55
#, kde-format
msgctxt "Agenda listview section title"
msgid "Todo"
msgstr "Da fare"

#: calendar/eventdatadecorator.cpp:57
#, kde-format
msgctxt "Means 'Other calendar items'"
msgid "Other"
msgstr "Altro"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "%1 event"
msgid_plural "%1 events"
msgstr[0] "Un evento"
msgstr[1] "%1 eventi"

#: calendar/qml/DayDelegate.qml:43
#, kde-format
msgid "No events"
msgstr "Nessun evento"

#: calendar/qml/MonthViewHeader.qml:67
#, kde-format
msgctxt "Format: month year"
msgid "%1 %2"
msgstr "%1 %2"

#: calendar/qml/MonthViewHeader.qml:113
#, kde-format
msgid "Days"
msgstr "Giorni"

#: calendar/qml/MonthViewHeader.qml:119
#, kde-format
msgid "Months"
msgstr "Mesi"

#: calendar/qml/MonthViewHeader.qml:125
#, kde-format
msgid "Years"
msgstr "Anni"

#: calendar/qml/MonthViewHeader.qml:163
#, kde-format
msgid "Previous Month"
msgstr "Mese precedente"

#: calendar/qml/MonthViewHeader.qml:165
#, kde-format
msgid "Previous Year"
msgstr "Anno precedente"

#: calendar/qml/MonthViewHeader.qml:167
#, kde-format
msgid "Previous Decade"
msgstr "Decennio precedente"

#: calendar/qml/MonthViewHeader.qml:184
#, kde-format
msgctxt "Reset calendar to today"
msgid "Today"
msgstr "Oggi"

#: calendar/qml/MonthViewHeader.qml:185
#, kde-format
msgid "Reset calendar to today"
msgstr "Ripristina il calendario a oggi"

#: calendar/qml/MonthViewHeader.qml:196
#, kde-format
msgid "Next Month"
msgstr "Mese successivo"

#: calendar/qml/MonthViewHeader.qml:198
#, kde-format
msgid "Next Year"
msgstr "Anno successivo"

#: calendar/qml/MonthViewHeader.qml:200
#, kde-format
msgid "Next Decade"
msgstr "Decennio successivo"

#: calendar/qml/MonthViewHeader.qml:256
#, kde-format
msgid "Keep Open"
msgstr "Mantieni aperto"

#: containmentlayoutmanager/qml/BasicAppletContainer.qml:270
#, kde-format
msgid "Configure…"
msgstr "Configura…"

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:10
#, kde-format
msgid "Screen lock enabled"
msgstr "Blocco dello schermo abilitato"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:11
#, kde-format
msgid "Sets whether the screen will be locked after the specified time."
msgstr "Imposta se lo schermo debba essere bloccato dopo il tempo indicato."

#. i18n: ectx: label, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:16
#, kde-format
msgid "Screen saver timeout"
msgstr "Scadenza per il salvaschermo"

#. i18n: ectx: whatsthis, entry, group (Daemon)
#: sessionsprivate/kscreenlockersettings.kcfg:17
#, kde-format
msgid "Sets the minutes after which the screen is locked."
msgstr "Imposta dopo quanti minuti lo schermo viene bloccato."

#: sessionsprivate/sessionsmodel.cpp:235 sessionsprivate/sessionsmodel.cpp:239
#, kde-format
msgid "New Session"
msgstr "Nuova sessione"

#: shellprivate/widgetexplorer/kcategorizeditemsviewmodels.cpp:64
#, kde-format
msgid "Filters"
msgstr "Filtri"

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:148
#, kde-format
msgid ""
"This Widget was written for an unknown older version of Plasma and is not "
"compatible with Plasma %1. Please contact the widget's author for an updated "
"version."
msgstr ""
"Questo oggetto è stato scritto per una versione sconosciuta più vecchia di "
"Plasma e non è compatibile con Plasma %1. Contatta l'autore/autrice "
"dell'oggetto per una versione aggiornata."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:152
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please contact the widget's author for an updated version."
msgstr ""
"Questo oggetto è stato scritto per Plasma %1 e non è compatibile con Plasma "
"%2. Contatta l'autore/autrice dell'oggetto per una versione aggiornata."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:156
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with Plasma %2. "
"Please update Plasma in order to use the widget."
msgstr ""
"Questo oggetto è stato scritto per Plasma %1e non è compatibile con Plasma "
"%2. Aggiorna Plasma per usare l'oggetto."

#: shellprivate/widgetexplorer/plasmaappletitemmodel.cpp:161
#, kde-format
msgid ""
"This Widget was written for Plasma %1 and is not compatible with the latest "
"version of Plasma. Please update Plasma in order to use the widget."
msgstr ""
"Questo oggetto è stato scritto per Plasma %1 e non è compatibile con "
"l'ultima versione di Plasma. Aggiorna Plasma per usare l'oggetto."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:111
msgctxt "applet category"
msgid "Accessibility"
msgstr "Accessibilità"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:112
msgctxt "applet category"
msgid "Application Launchers"
msgstr "Avviatori di applicazioni"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:113
msgctxt "applet category"
msgid "Astronomy"
msgstr "Astronomia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:114
msgctxt "applet category"
msgid "Date and Time"
msgstr "Data e ora"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:115
msgctxt "applet category"
msgid "Development Tools"
msgstr "Strumenti di sviluppo"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:116
msgctxt "applet category"
msgid "Education"
msgstr "Istruzione"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:117
msgctxt "applet category"
msgid "Environment and Weather"
msgstr "Ambiente e meteo"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:118
msgctxt "applet category"
msgid "Examples"
msgstr "Esempi"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:119
msgctxt "applet category"
msgid "File System"
msgstr "Filesystem"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:120
msgctxt "applet category"
msgid "Fun and Games"
msgstr "Divertimento e giochi"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:121
msgctxt "applet category"
msgid "Graphics"
msgstr "Grafica"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:122
msgctxt "applet category"
msgid "Language"
msgstr "Lingua"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:123
msgctxt "applet category"
msgid "Mapping"
msgstr "Mappe"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:124
msgctxt "applet category"
msgid "Miscellaneous"
msgstr "Varie"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:125
msgctxt "applet category"
msgid "Multimedia"
msgstr "Multimedia"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:126
msgctxt "applet category"
msgid "Online Services"
msgstr "Servizi in rete"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:127
msgctxt "applet category"
msgid "Productivity"
msgstr "Produttività"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:128
msgctxt "applet category"
msgid "System Information"
msgstr "Informazioni di sistema"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:129
msgctxt "applet category"
msgid "Utilities"
msgstr "Accessori"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:130
msgctxt "applet category"
msgid "Windows and Tasks"
msgstr "Finestre e attività"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:131
msgctxt "applet category"
msgid "Clipboard"
msgstr "Appunti"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:132
msgctxt "applet category"
msgid "Tasks"
msgstr "Attività"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:148
#, kde-format
msgid "All Widgets"
msgstr "Tutti gli oggetti"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:152
#, kde-format
msgid "Running"
msgstr "In esecuzione"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:158
#, kde-format
msgctxt ""
"@item:inmenu used in the widget filter. Filter widgets that can be un-"
"installed from the system, which are usually installed by the user to a "
"local place."
msgid "Uninstallable"
msgstr "Disinstallabile"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:162
#, kde-format
msgid "Categories:"
msgstr "Categorie:"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:232
#, kde-format
msgid "Download New Plasma Widgets"
msgstr "Scarica nuovi oggetti di Plasma"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:241
#, kde-format
msgid "Install Widget From Local File…"
msgstr "Installa oggetto da file locale…"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:503
#, kde-format
msgid "Select Plasmoid File"
msgstr "Seleziona un file di plasmoide"

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installing the package %1 failed."
msgstr "Installazione del pacchetto %1 non riuscita."

#: shellprivate/widgetexplorer/widgetexplorer.cpp:517
#, kde-format
msgid "Installation Failure"
msgstr "Installazione non riuscita"

#~ msgid "&Execute"
#~ msgstr "&Esegui"

#~ msgctxt "Toolbar Button to switch to Plasma Scripting Mode"
#~ msgid "Plasma"
#~ msgstr "Plasma"

#~ msgctxt "Toolbar Button to switch to KWin Scripting Mode"
#~ msgid "KWin"
#~ msgstr "KWin"

#~ msgid "Templates"
#~ msgstr "Modelli"

#~ msgid "Desktop Shell Scripting Console"
#~ msgstr "Console di creazione script della shell per il desktop"

#~ msgid "Editor"
#~ msgstr "Editor"

#~ msgid "Load"
#~ msgstr "Carica"

#~ msgid "Use"
#~ msgstr "Usa"

#~ msgid "Output"
#~ msgstr "Output"

#~ msgid "Unable to load script file <b>%1</b>"
#~ msgstr "Impossibile caricare il file di script <b>%1</b>"

#~ msgid "Open Script File"
#~ msgstr "Apri file di script"

#~ msgid "Save Script File"
#~ msgstr "Salva file di script"

#~ msgid "Executing script at %1"
#~ msgstr "Esecuzione script alle %1"

#~ msgid "Runtime: %1ms"
#~ msgstr "Tempo di esecuzione: %1ms"

#~ msgid "Download Wallpaper Plugins"
#~ msgstr "Estensioni per lo scaricamento di sfondi"
