# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Elkana Bardugo <ttv200@gmail.com>, 2016.
# Elkana Bardugo <ttv200@gmail.com>, 2017. #zanata
# SPDX-FileCopyrightText: 2023 Yaron Shahrabani <sh.yaron@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma_engine_devicenotifications\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-15 00:20+0000\n"
"PO-Revision-Date: 2023-10-22 08:38+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: צוות התרגום של KDE ישראל\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Lokalize 23.08.1\n"

#: ksolidnotify.cpp:149
#, kde-format
msgid "Device Status"
msgstr "מצב התקן"

#: ksolidnotify.cpp:149
#, kde-format
msgid "A device can now be safely removed"
msgstr "אפשר לנתק את ההתקן בבטחה כעת"

#: ksolidnotify.cpp:150
#, kde-format
msgid "This device can now be safely removed."
msgstr "אפשר לנתק את ההתקן בבטחה."

#: ksolidnotify.cpp:157
#, kde-format
msgid "You are not authorized to mount this device."
msgstr "אסור לך לעגן את ההתקן."

#: ksolidnotify.cpp:160
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "You are not authorized to remove this device."
msgstr "אין לך הרשאה לניתוק ההתקן."

#: ksolidnotify.cpp:163
#, kde-format
msgid "You are not authorized to eject this disc."
msgstr "אין לך הרשאה לשליפת הדיסק."

#: ksolidnotify.cpp:170
#, kde-format
msgid "Could not mount this device as it is busy."
msgstr "אי אפשר לעגן את ההתקן כשהוא עסוק."

#: ksolidnotify.cpp:200
#, kde-format
msgid "One or more files on this device are open within an application."
msgstr "קובץ אחד או יותר בהתקן פתוח ביישום."

#: ksolidnotify.cpp:202
#, kde-format
msgid "One or more files on this device are opened in application \"%2\"."
msgid_plural ""
"One or more files on this device are opened in following applications: %2."
msgstr[0] "קובץ אחד או יותר מההתקן פתוח ביישום „%2”."
msgstr[1] "קובץ אחד או יותר מההתקן פתוח ביישומים הבאים: %2."
msgstr[2] "קובץ אחד או יותר מההתקן פתוח ביישומים הבאים: %2."
msgstr[3] "קובץ אחד או יותר מההתקן פתוח ביישומים הבאים: %2."

#: ksolidnotify.cpp:205
#, kde-format
msgctxt "separator in list of apps blocking device unmount"
msgid ", "
msgstr ", "

#: ksolidnotify.cpp:222
#, kde-format
msgid "Could not mount this device."
msgstr "אי אפש לעגן את ההתקן."

#: ksolidnotify.cpp:225
#, kde-format
msgctxt "Remove is less technical for unmount"
msgid "Could not remove this device."
msgstr "אי אפשר לנתק את ההתקן."

#: ksolidnotify.cpp:228
#, kde-format
msgid "Could not eject this disc."
msgstr "אי אפשר לשלוף את הדיסק."
