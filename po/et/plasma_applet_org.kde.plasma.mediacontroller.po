# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Marek Laane <qiilaq69@gmail.com>, 2016, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-26 01:34+0000\n"
"PO-Revision-Date: 2019-10-30 22:25+0200\n"
"Last-Translator: Marek Laane <qiilaq69@gmail.com>\n"
"Language-Team: Estonian <kde-et@lists.linux.ee>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.1\n"

#: package/contents/ui/AlbumArtStackView.qml:202
#, kde-format
msgid "No title"
msgstr ""

#: package/contents/ui/AlbumArtStackView.qml:202
#: package/contents/ui/main.qml:54
#, kde-format
msgid "No media playing"
msgstr "Midagi ei esitata"

#: package/contents/ui/ExpandedRepresentation.qml:383
#: package/contents/ui/ExpandedRepresentation.qml:505
#, kde-format
msgctxt "Remaining time for song e.g -5:42"
msgid "-%1"
msgstr "-%1"

#: package/contents/ui/ExpandedRepresentation.qml:533
#, kde-format
msgctxt "@action:button"
msgid "Shuffle"
msgstr ""

#: package/contents/ui/ExpandedRepresentation.qml:557
#: package/contents/ui/main.qml:111
#, kde-format
msgctxt "Play previous track"
msgid "Previous Track"
msgstr "Eelmine pala"

#: package/contents/ui/ExpandedRepresentation.qml:579
#: package/contents/ui/main.qml:119
#, kde-format
msgctxt "Pause playback"
msgid "Pause"
msgstr "Paus"

#: package/contents/ui/ExpandedRepresentation.qml:579
#: package/contents/ui/main.qml:127
#, kde-format
msgctxt "Start playback"
msgid "Play"
msgstr "Esita"

#: package/contents/ui/ExpandedRepresentation.qml:597
#: package/contents/ui/main.qml:135
#, kde-format
msgctxt "Play next track"
msgid "Next Track"
msgstr "Järgmine pala"

#: package/contents/ui/ExpandedRepresentation.qml:620
#, fuzzy, kde-format
#| msgctxt "Play next track"
#| msgid "Next Track"
msgid "Repeat Track"
msgstr "Järgmine pala"

#: package/contents/ui/ExpandedRepresentation.qml:620
#, kde-format
msgid "Repeat"
msgstr ""

#: package/contents/ui/main.qml:57
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (%2)\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr "esitajalt %1 (pausi peal, %2)"

#: package/contents/ui/main.qml:58
#, kde-format
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"%1\n"
"Middle-click to pause\n"
"Scroll to adjust volume"
msgstr ""

#: package/contents/ui/main.qml:60
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "@info:tooltip %1 is a musical artist and %2 is an app name"
msgid ""
"by %1 (paused, %2)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr "esitajalt %1 (pausi peal, %2)"

#: package/contents/ui/main.qml:61
#, fuzzy, kde-format
#| msgctxt "by Artist (paused, player name)"
#| msgid "by %1 (paused, %2)"
msgctxt "@info:tooltip %1 is an app name"
msgid ""
"Paused (%1)\n"
"Middle-click to play\n"
"Scroll to adjust volume"
msgstr "esitajalt %1 (pausi peal, %2)"

#: package/contents/ui/main.qml:104
#, kde-format
msgctxt "Open player window or bring it to the front if already open"
msgid "Open"
msgstr "Ava"

#: package/contents/ui/main.qml:143
#, kde-format
msgctxt "Stop playback"
msgid "Stop"
msgstr "Peata"

#: package/contents/ui/main.qml:156
#, kde-format
msgctxt "Quit player"
msgid "Quit"
msgstr "Välju"

#~ msgid "Choose player automatically"
#~ msgstr "Mängija automaatne valimine"

#~ msgctxt "by Artist (player name)"
#~ msgid "by %1 (%2)"
#~ msgstr "esitajalt %1 (%2)"

#~ msgctxt "Paused (player name)"
#~ msgid "Paused (%1)"
#~ msgstr "Pausi peal (%1)"

#~ msgctxt "artist – track"
#~ msgid "%1 – %2"
#~ msgstr "%1 – %2"

#~ msgctxt "Artist of the song"
#~ msgid "by %1"
#~ msgstr "esitajalt %1"
